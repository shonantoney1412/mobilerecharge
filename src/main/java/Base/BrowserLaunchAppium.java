package Base;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BrowserLaunchAppium {
public static AppiumDriver getAppiumDriver() {
	AppiumDriver driver =null;
	try {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "MONSTERFOREVER");
		cap.setCapability("udid", "24086706"); // 192.168.1.2:5555 //24086706
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "11");
		Thread.sleep(40000);
		cap.setCapability("automationName", "UiAutomator2");
		cap.setCapability("appPackage", "com.phonepe.app");
		cap.setCapability("appActivity", "com.phonepe.app.v4.nativeapps.splash.Navigator_SplashActivity");
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url, cap);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	return driver;
	
}
	
}
