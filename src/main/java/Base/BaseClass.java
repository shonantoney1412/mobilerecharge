package Base;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseClass {

	public static AppiumDriver<MobileElement> driver;
	public static DesiredCapabilities cap;

	public static void Android_LaunchApp() throws Exception {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "MONSTERFOREVER");
		cap.setCapability("udid", "24086706"); // 192.168.1.2:5555 //24086706
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "11");
		// Thread.sleep(40000);
		cap.setCapability("automationName", "UiAutomator2");
		cap.setCapability("appPackage", "com.phonepe.app");
		cap.setCapability("appActivity", "com.phonepe.app.v4.nativeapps.splash.Navigator_SplashActivity");
//		URL url = new URL("http://127.0.0.1:4723/wd/hub");
//		driver = new AppiumDriver<MobileElement>(url, cap);

		PomClass.setDriver(driver);
	}

	public static void geturl(String url) {
		driver.get(url);
		
	}
	
	public static void clickonElement(MobileElement element) {
		element.click();
	}
	
	public static void valueElement(MobileElement value) {
		
		value.sendKeys("values");
	}
	
public static void sleep() throws InterruptedException{
	Thread.sleep(5000);
	}


	public static void closeApp() {
		driver.quit();
	}

}
