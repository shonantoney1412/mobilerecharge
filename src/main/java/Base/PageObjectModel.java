package Base;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Base.PomClass;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PageObjectModel extends BasePage {

	// public static AppiumDriver<MobileElement> driver;

	public PageObjectModel(AppiumDriver driver) {
		// PageFactory.initElements(new AppiumFieldDecorator(PhonePE)

super(driver);
		// PageFactory.initElements(new AppiumFieldDecorator(PhonePE.getDriver()),
		// This);

		PageFactory.initElements(new AppiumFieldDecorator(PomClass.getdrDriver()), this);
	}



	@FindBy(id = "com.google.android.gms:id/cancel")
	private WebElement addnumber;

	@FindBy(id = "com.phonepe.app:id/et_mobile_number")
	private WebElement mobilenumber;

	@FindBy(id = "com.phonepe.app:id/tv_verify")
	private WebElement proceed;

	@FindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
	private WebElement confirm;

	@FindBy(id = "com.android.permissioncontroller:id/permission_allow_one_time_button")
	private WebElement location;

	@FindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
	private WebElement contacts;

	@FindBy(id = "com.android.permissioncontroller:id/permission_allow_button")
	private WebElement phonecalls;

	@FindBy(xpath = "//android.widget.TextView[@text = 'Mobile Recharge' and @index='1']")
	private WebElement recharge;

	@FindBy(xpath = "//android.widget.TextView[@text = '7305451441' and @index='1']")
	private WebElement mobnum;

	@FindBy(xpath = "//android.widget.TextView[@text = 'Jio Prepaid' and @index ='4']")
	private WebElement jio;

	@FindBy(xpath = "//android.widget.TextView[@text ='Airtel Prepaid' and @index = '1']")
	private WebElement airtel;

	@FindBy(xpath = "//android.widget.TextView[@text = 'Chennai' and @index = '0']")
	private WebElement chennai;

	@FindBy(xpath = "//android.widget.EditText[@text = 'Search for a plan, eg 249 or 28 days' and @index = '1']")
	private WebElement plan;

	@FindBy(xpath = "//android.widget.TextView[@text = 'Talktime :' and @index = '0']")
	private WebElement talktime;

	@FindBy(id = "com.phonepe.app:id/tv_add_another_bank")
	private WebElement bankacc;

	@FindBy(xpath = "//android.widget.TextView[@text = 'Axis Bank' and @index = '1']")
	private WebElement axis;

	@FindBy(id = "com.phonepe.app:id/et_card_number")
	private WebElement card;

	@FindBy(id = "com.phonepe.app:id/et_card_expiry_month")
	private WebElement exmon;

	@FindBy(id = "com.phonepe.app:id/et_card_expiry_year")
	private WebElement exyear;

	@FindBy(id = "com.phonepe.app:id/et_card_cvv")
	private WebElement ccv;

	@FindBy(id = "com.phonepe.app:id/pay_button_container")
	private WebElement cont;

	@FindBy(id = "com.phonepe.app:id/progress_action_button")
	private WebElement payment;

	
	
	public void recharge() {
		addnumber.click();
		mobilenumber.sendKeys("7304541441");
		proceed.click();
		confirm.click();
		location.click();
		contacts.click();
		phonecalls.click();
		recharge.click();
		mobnum.click();
		jio.click();
		airtel.click();
		chennai.click();
		plan.click();
		talktime.click();
		bankacc.click();
		axis.click();
		card.sendKeys("6522360054310895");
		exmon.sendKeys("12");
		exyear.sendKeys("22");
		ccv.sendKeys("123");
		cont.click();
		payment.click();

	}

}
