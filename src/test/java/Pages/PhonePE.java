package Pages;

import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class PhonePE {

	static AppiumDriver<MobileElement> driver;

	public static void main(String[] args) {
		try {
			openPhonePe();
		} catch (Exception e) {
			e.getCause();
			e.getMessage();
			e.printStackTrace();
		}

	}

	public static void openPhonePe() throws Exception {

		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "MONSTERFOREVER");
		cap.setCapability("udid", "24086706"); // 192.168.1.2:5555 //24086706
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "11");
		 //Thread.sleep(40000);
		cap.setCapability("automationName", "UiAutomator2");
		cap.setCapability("appPackage", "com.phonepe.app");
		cap.setCapability("appActivity", "com.phonepe.app.v4.nativeapps.splash.Navigator_SplashActivity");

		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url, cap);
		System.out.println("Application Started");

		driver.findElement(By.id("com.google.android.gms:id/cancel")).click(); // add number
		driver.findElement(By.id("com.phonepe.app:id/et_mobile_number")).sendKeys("7305451441");
		driver.findElement(By.id("com.phonepe.app:id/tv_verify")).click();
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button")).click(); // sms
		Thread.sleep(15000);
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_one_time_button")).click(); // location
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button")).click(); // contacts
		driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button")).click(); // phonecalls
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.widget.TextView[@text = 'Mobile Recharge' and @index='1']")).click();
		Thread.sleep(8000);
		driver.findElement(By.xpath("//android.widget.TextView[@text = '7305451441' and @index='1']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//android.widget.TextView[@text = 'Jio Prepaid' and @index ='4']")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text ='Airtel Prepaid' and @index = '1']")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text = 'Chennai' and @index = '0']")).click();
		driver.findElement(By.xpath("//android.widget.EditText[@text = 'Search for a plan, eg 249 or 28 days' and @index = '1']")).sendKeys("10");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.widget.TextView[@text = 'Talktime :' and @index = '0']")).click(); // confirm
		driver.findElement(By.id("com.phonepe.app:id/tv_add_another_bank")).click();
		driver.findElement(By.xpath("//android.widget.TextView[@text = 'Axis Bank' and @index = '1']")).click();
		driver.findElement(By.id("com.phonepe.app:id/et_card_number")).sendKeys("6522360054310895");
		driver.findElement(By.id("com.phonepe.app:id/et_card_expiry_month")).sendKeys("12");
		driver.findElement(By.id("com.phonepe.app:id/et_card_expiry_year")).sendKeys("22");
		driver.findElement(By.id("com.phonepe.app:id/et_card_cvv")).sendKeys("123");
		driver.findElement(By.id("com.phonepe.app:id/pay_button_container")).click();
		driver.findElement(By.id("com.phonepe.app:id/progress_action_button")).click(); // continue for payment

	}

}
