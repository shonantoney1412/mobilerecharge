package TestCase;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class Calculator {

	 //WebDriver driver;
	static AppiumDriver<MobileElement> driver;
	//AndroidDriver driver;
	public static void main(String[] args) {
		
		try {
			openCalculator();
		} catch (Exception e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
	}
	public static void openCalculator() throws Exception { 
		
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "MONSTERFOREVER");
		cap.setCapability("udid", "24086706");
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "11");
		Thread.sleep(8000);
		cap.setCapability("automationName", "UiAutomator2");;
		cap.setCapability("appPackage", "com.oneplus.calculator");
		cap.setCapability("appActivity", "com.oneplus.calculator.Calculator");
		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		
		driver = new AppiumDriver<MobileElement>(url, cap);
		
		System.out.println("Its working");
	}
	

}
