package TestCase;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Appiumcode {
	
	
	 static AndroidDriver<MobileElement> driver;
	
	
	public static void main(String[] args) throws Exception {
		

	
	DesiredCapabilities de = new DesiredCapabilities();

	de.setCapability(MobileCapabilityType.DEVICE_NAME, "ShOn");
	de.setCapability("udid", "24086706");
    de.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");	
	de.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11");
	//de.setCapability(MobileCapabilityType.BROWSER_NAME, "");
	//de.setCapability(MobileCapabilityType.APP, "C:\\Program Files\\Appium Server GUI\\PhonePE\\phonepe.app");
	//de.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
	//de.setCapability("appActivity", "com.phonepe.app.ui.activity.Navigator_MainActivity - PhonePe");
	
	
	//URL url = new URL("http://127.0.0.1:4723/wd/hub");
	//driver = new AppiumDriver<MobileElement>(url, de);
	driver = new AndroidDriver<MobileElement> (new URL("http://127.0.0.1:4723/wd/hub"), de);
	System.out.println("Application Started");
	
	}
}
